//Переключення табів в секції "Gallery".
const $tabsTitle = $('.gallery-tabs-title');
const $tabsContent = $('.gallery-tab');

$tabsTitle.click(function () {
    $(this).addClass('active')
    .siblings()
    .removeClass('active');
    $tabsContent.removeClass('active')
    .eq($(this).index())
    .addClass('active');
    $('.gallery-tab-img-wrapper.focus').removeClass('focus');
    $('.gallery-tab.active .gallery-tab-img-wrapper').first()
    .addClass('focus');
    showFullInfo();
});

//Карусель  в секції "Gallery".
(function () {
    showStars();
    showInfo();
}());

$('.gallery-tab-next-btn').click(function (event) {
    event.preventDefault();
    let $selectedItems = $('.gallery-tab.active .gallery-tab-img-wrapper.focus');
    if (($selectedItems.next()).length === 0) {
        $('.gallery-tab.active .gallery-tab-img-wrapper').first()
        .addClass('focus')
        .siblings()
        .removeClass('focus');
    } else {
        $selectedItems.next()
        .addClass('focus')
        .siblings()
        .removeClass('focus');
    };
    showFullInfo();
});

$('.gallery-tab-previous-btn').click(function (event) {
    event.preventDefault();
    let $selectedItems = $('.gallery-tab.active .gallery-tab-img-wrapper.focus');
    if (($selectedItems.prev()).length === 0) {
        $('.gallery-tab.active .gallery-tab-img-wrapper').last()
        .addClass('focus')
        .siblings()
        .removeClass('focus');
    } else {
        $selectedItems.prev()
        .addClass('focus')
        .siblings()
        .removeClass('focus');
    };
    showFullInfo();
});

$('.gallery-tab-img-wrapper').click(function (event) {
    $('.gallery-tab-img-wrapper').removeClass('focus');
    $(this).addClass('focus');
    showFullInfo();
});

function cleanStars() {
    $('.gallery-tab-text-stars .fas.fa-star').removeClass('fas').addClass('far');
};

function showStars() {
    let stars = $('.focus .gallery-tab-img').attr('data-stars');
    for (let i = 0; i < stars; i++) {
        $('.gallery-tab-text-stars .far.fa-star').first().removeClass('far').addClass('fas');
    };
};

function showInfo() {
    $('.gallery-tab-text-title').html(`${$('.focus .gallery-tab-img').attr('data-title')}`);
    $('.gallery-tab-text-price').html(`${$('.focus .gallery-tab-img').attr('data-price')}`);
    if($('.focus .gallery-tab-img').attr('data-old-price')) {
        $('.gallery-tab-price').css({'padding-top': '37px'});
        $('.gallery-tab-text-old-price').html(`${$('.focus .gallery-tab-img').attr('data-old-price')}`);
    } else {
        $('.gallery-tab-price').css({'padding-top': '48px'});
        $('.gallery-tab-text-old-price').html('');
    }
    $('.gallery-tab-big-img').attr('src', `${$('.focus .gallery-tab-img').attr('src')}`);
};

function showFullInfo() {
    cleanStars();
    showStars();
    showInfo();
};

//Включення тултипів
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});