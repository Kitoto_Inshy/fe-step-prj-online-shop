// Перевірка наявності товарів у кошику
document.addEventListener('DOMContentLoaded', (() => {
    if (!localStorage.getItem('items')) {
        localStorage.setItem('items', '0');
        localStorage.setItem('cart', '');
        $('.header-cart__amount').html(`${localStorage.getItem('items')}`);
    } else {
        $('.header-cart__amount').html(`${localStorage.getItem('items')}`);
    }
}));

// Додавання товарів до кошику з Gallery
$('.cart-add-btn').click(function () {
    localStorage.setItem('items', `${+localStorage.getItem('items') + 1}`);
    $('.header-cart__amount').html(`${localStorage.getItem('items')}`);
    const $addedItem = $('.focus .gallery-tab-img');
    localStorage.setItem('cart', [localStorage.getItem('cart') + $addedItem.attr('id') + ","]);
});

// Додавання товарів до кошику з Prodacts
$('.furniture__add-to-cart').click(function () {
    localStorage.setItem('items', `${+localStorage.getItem('items') + 1}`);
    $('.header-cart__amount').html(`${localStorage.getItem('items')}`);
    const $addedItem = $(this).prev();
    localStorage.setItem('cart', [localStorage.getItem('cart') + $addedItem.attr('id') + ","]);
});

// Прегляд товарів у кошику
$('.header-cart').click(function () {
    const inCart = document.querySelectorAll('div .modal-cart-info');
    inCart.forEach(elem => elem.remove());
    if (+localStorage.getItem('items')) {
        $('.modal-cart-empty').addClass('hide-cart');
        $('.modal-cart-help-div').css({ display: 'block' });
        let cartAmountItems = localStorage.getItem('cart').split(',');
        cartAmountItems.length -= 1;
        const modalCartTotalPrice = document.querySelector('.modal-cart-help-div');
        for (let i = 0; i < cartAmountItems.length; i++) {
            let $itemId = $(`#${cartAmountItems[i]}`);
            if ($itemId.attr('id') > 'item-020') {
                $itemId.attr('src', `${$itemId.attr('data-src')}`);
                $itemId.attr('data-title', `${$itemId.attr('data-name')}`);
                if ($itemId.attr('data-crossed')) {
                    $itemId.attr('data-old-price', `${$itemId.attr('data-crossed')}`);
                } else {
                    $itemId.attr('data-old-price', ``);
                };
            };
            const modalCartItem = document.createElement('div');
            modalCartItem.innerHTML = `<div class="modal-cart-info">
        <div class="modal-cart-info-img">
            <img src="${$itemId.attr('src')}">
        </div>
        <h4 class="modal-cart-info-title">${$itemId.attr('data-title')}</h4>
        <p class="modal-cart-info-old-price">${$itemId.attr('data-old-price')}</p>
        <p class="modal-cart-info-price">${$itemId.attr('data-price')}</p>
        <div class="modal-cart-info-sum">
            <p class="modal-cart-info-sum-text">Sum</p>
            <p class="modal-cart-info-sum-cash">
            ${$itemId.attr('data-price')}
            </p>
        </div>
        <div class="modal-cart-info-quantity">
            <p class="modal-cart-info-quantity-text">Quantity:</p>
            <p class="modal-cart-info-quantity-amount">1</p>
            <button class="modal-cart-info-quantity-btn">
                <i class="fas fa-minus"></i>
            </button>
            <button class="modal-cart-info-quantity-btn">
                <i class="fas fa-plus"></i>
            </button>
        </div>
        </div>`;
            modalCartTotalPrice.before(modalCartItem);
        };
    };
});

