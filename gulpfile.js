const gulp = require('gulp'),
	sass = require('gulp-sass'),
	sync = require('browser-sync'),
	rigger = require('gulp-rigger'),
	concat = require('gulp-concat'),
	clean = require('gulp-clean'),
	imagemin = require('gulp-imagemin'),
	htmlmin = require('gulp-htmlmin'),
	minify = require('gulp-minify');

const paths = {
	htmlForRigger: 'src/index.html',
	htmlForWatch: "src/templates/*.html",
	css: ['src/sass/**/*.sass', 'src/sass/**/*.scss'],
	js: 'src/js/**/*.js',
	imgs: ["src/img/**/**.jpg", "src/img/**/**.png"],
	fonts: ["src/fonts/**.eot", "src/fonts/**.ttf",
	"src/fonts/**.woff", "src/fonts/**.otf"],
	distHtml: './dist',
	distCss: './dist/css',
	distJs: './dist/js',
	distImg: './dist/img',
	distFonts: './dist/fonts'
}

gulp.task('scripts', function () {
	return gulp.src(paths.js)
		.pipe(concat('main.js'))	
		.pipe(gulp.dest(paths.distJs))
		.pipe(sync.reload({ stream: true }))
});

gulp.task('minscripts', function () {
	return gulp.src(paths.js)
		.pipe(concat('main.js'))
		.pipe(minify())
		.pipe(gulp.dest(paths.distJs));
});

gulp.task('sass', function () {
	return gulp.src(paths.css)
		.pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
		.pipe(gulp.dest(paths.distCss))
		.pipe(sync.reload({ stream: true }))
});

gulp.task('mincss', function () {
	return gulp.src(paths.css)
		.pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
		.pipe(gulp.dest(paths.distCss))
		.pipe(sync.reload({ stream: true }))
});

gulp.task('html', function () {
	return gulp.src(paths.htmlForRigger)
		.pipe(rigger())
		.pipe(gulp.dest(paths.distHtml))
		.pipe(sync.reload({ stream: true }));
});

gulp.task('minhtml', function () {
	return gulp.src(paths.htmlForRigger)
		.pipe(rigger())
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest(paths.distHtml));
});

gulp.task('imgs', function () {
	return gulp.src(paths.imgs)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.distImg))
		.pipe(sync.reload({ stream: true }));
});

gulp.task('fonts', function () {
	return gulp.src(paths.fonts)
		.pipe(gulp.dest(paths.distFonts));
});

gulp.task('watch', function () {
	gulp.watch(paths.css, gulp.series('sass'));
	gulp.watch(paths.htmlForWatch, gulp.series('html'));
	gulp.watch(paths.imgs, gulp.series('imgs'));
});

gulp.task('clean_dist', function () {
	return gulp.src('./dist', { read: false })
		.pipe(clean())
});

gulp.task('sync', function () {
	sync.init({
		server: {
			baseDir: 'dist'
		}
	})
});

gulp.task('start', gulp.series('clean_dist', 'fonts', 'imgs', 'sass', 'html', 'scripts'));
gulp.task('build', gulp.series('clean_dist', 'fonts', 'imgs', 'minhtml', 'mincss', 'minscripts'));
gulp.task('watch_sync', gulp.parallel('watch', 'sync'));
gulp.task('default', gulp.series('start', 'watch_sync'));